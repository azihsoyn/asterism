# encoding: utf-8
namespace :feed do
    desc "feedの更新処理"
    task :update => :environment do
      Resque.enqueue(FeedUpdater, nil) 
    end
end
