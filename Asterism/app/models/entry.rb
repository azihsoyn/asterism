class Entry < ActiveRecord::Base
  attr_accessible :author, :categories, :content, :feed_id, :published, :summary, :title, :url, :user_id
  belongs_to :feed
  # for star
  has_many :stared_entries
  has_many :users, :through => :stared_entries
  # for read
  has_many :read_entries
  has_many :users, :through => :read_entries
end
