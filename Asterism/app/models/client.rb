class Client
  def self.make(token, secret)
    Twitter::Client.new(
      :oauth_token => token,
      :oauth_token_secret => secret
    )
  end
end
