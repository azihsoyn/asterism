class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :omniauthable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :uid, :name, :provider, :entry_id, :login, :image_url
  attr_accessor   :login
  # attr_accessible :title, :body
  validates :email,    :presence => true, :uniqueness => {:scope => [:uid, :provider]}
  validates :password, :presence => true, :length => {:minimun => 8, :maximum => 128}
  validates_format_of     :email, :with  => Asterism::Application.config.email_regexp

  has_many :user_feeds
  has_many :feeds, :through => :user_feeds
  has_many :entries, :through => :feeds, :order => "entries.published DESC"

  # for star
  has_many :stared_entries
  has_many :entries_with_star, :through => :stared_entries, :class_name => "Entry"

  # for read
  has_many :read_entries
  has_many :entries_with_read, :through => :read_entries, :class_name => "Entry"

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"]
      end
    end
  end

  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.new(name:auth.extra.raw_info.name,
               provider:auth.provider,
               uid:auth.uid,
               email:auth.info.email, #emailを取得したい場合は、migrationにemailを追加してください。
               password:Devise.friendly_token[0,20],
               image_url:auth.info.image
             )
      user.skip_confirmation!
      user.save
    end
    user
  end


  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.new(name:auth.info.nickname,
               provider:auth.provider,
               uid:auth.uid,
               email:auth.info.nickname+"@twitter.com",# email:auth.extra.user_hash.email, #色々頑張りましたがtwitterではemail取得できません
               password:Devise.friendly_token[0,20],
               image_url:auth.extra.raw_info.profile_image_url_https
             )
      user.skip_confirmation!
      user.save
    end
    user
  end
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["(lower(name) = :value OR lower(email) = :value) and provider = 'asterism'", { :value => login.downcase }]).first
    else
      where(conditions).first
    end
  end
 
end
