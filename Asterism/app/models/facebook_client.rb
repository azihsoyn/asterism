class FacebookClient
  def self.make(token)
    Koala::Facebook::API.new(
      token
    )
  end
end
