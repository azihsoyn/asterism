class FacebookGetData
  def initialize client
    @client = client
    info = @client.get_object("me")
    @id = info["id"]
    p @id
  end

  def run #1歩分のユーザー情報と、2歩分のフォロー情報をDBに格納するメソッド
    fetchMyData
    fetchFriendsData
  end

  def fetchMyData
    #fetchAndSaveInfo @id
    fetchAndSaveEdges @id
  end
#  def fetchFriendsData
#    friends = getEdgesFromDB @id
#    friends.each do |id|
#      fetchAndSaveInfo id
#      fetchAndSaveEdges id
#    end
#  end
  def fetchFriendsData
    friends = getEdgesFromDB @id
    installed = @client.get_connections("me", "friends", :fields => "id,name,installed")
    p "========== installed =========="
    p installed
    p installed.size
    Parallel.each(installed, :in_threads=> friends.size * 2) do |info|
      #fetchAndSaveInfo id
      if info["installed"] then
        p "============= installed =========="
        p info
        fetchAndSaveEdges info["id"]
      end
    end
  end


  def fetchAndSaveInfo id
    info = fetchInfo(id)
    saveInfo(id, info) if info
    true
  end

  def fetchAndSaveEdges id
    edges = fetchEdges(id)
    saveEdges(id, edges) if edges and edges.size > 0
    true
  end

  def fetchInfo id
    id = id.to_i
    begin
      info = @client.get_object("me")
    rescue => exc
      case exc.class.to_s
      when Koala::Facebook::APIError #そのユーザーが居ない場合
        delNode id
        return false
      else
        raise exc.to_s + id.to_s
      end
    end
    info
  end

  def fetchEdges id
    id = id.to_i
    begin
      my_follows = []
      friends_info = @client.get_connections(id, "friends")
      friends_info.each do |info|
          my_follows << info["id"]
      end
    rescue => exc
      case exc.class.to_s
      when Koala::Facebook::APIError #そのユーザーが居ない場合
        delNode id
        return false
      else
        raise exc.to_s + id.to_s
      end
    end
    my_follows
  end

  def info_key str
    str.to_s + "_info"
  end

  def edges_key str
    str.to_s + "_edges"
  end

  def delNode id
    $redis.del info_key(id)
    $redis.del edges_key(id)
  end

  def saveInfo id, info
    key = info_key(id)
    p "~============== key ============"
    p key
    info = info.to_hash
    p "=============== info ==========="
    p info
    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.ping

    $redis.del key
    $redis.mapped_hmset key, info
  end

  def saveEdges id, edges
    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.ping
    key = edges_key(id)
    edges = edges.to_a
    p "========== edges ======="
    p edges
    $redis.del key
    $redis.sadd key, edges
  end

  def getInfoFromDB id
    $redis.hgetall info_key(id)
  end

  def getEdgesFromDB id
    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.ping
    $redis.smembers edges_key(id)
  end



end
