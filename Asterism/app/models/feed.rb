class Feed < ActiveRecord::Base
  attr_accessible :created_at, :etag, :feed_url, :id, :last_modified, :site_url, :title, :updated_at
  has_many :user_feeds
  has_many :users, :through => :user_feeds
  has_many :entries
end
