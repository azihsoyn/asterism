class FeedUpdater
  @queue = :update_feeds
  def self.perform(session)
    begin
      p "background running"
      p "updating feeds..."
      _feedUrls = []
      _feeds = Feed.find(:all)
      _feeds.each do |feed|
        _feedUrls << feed.feed_url
      end
      p "feedUrls : "
      p _feedUrls
      _feeds = []
      _tmp = Feedzirra::Feed.fetch_and_parse(_feedUrls)
      _tmp.each do |_feed_url, _feed|
        _feeds << Feed.new(:feed_url => _feed_url, :site_url => _feed.url, :title => _feed.title, :etag => _feed.etag, :last_modified => _feed.last_modified) if _feed != 503
      end
      #p _feeds
      if Feed.import(_feeds, { :on_duplicate_key_update => [:title, :site_url, :etag, :last_modified ] })
          p "success"
        # entries bulk insert
        _entries = [] #Array.newでも可
        _tmp.each do |_feed_url, _feed|
            _feedId = Feed.find_by_feed_url(_feed_url).id
            next if _feed == 503
            _feed.entries.each do |entry|
              p entry
              _entries << Entry.new(:url => entry.url, :content => ActionView::Base.white_list_sanitizer.sanitize(entry.content), :summary => entry.summary, :author => entry.author, :categories => entry.categories, :title => entry.title, :published => entry.published, :feed_id => _feedId)
            end
        end

        if Entry.import(_entries, { :on_duplicate_key_update => [:title, :published, :author, :summary, :content, :categories, :feed_id]})
            p "success 2"
        end
      end

    rescue => exc
        p "error"
      p exc
    end
  end
end
