class RunProcessor
  @queue = :fetch_sns
  def self.perform(session)
    begin
      p "background running"
      p "provider : "
      p session
      p session["provider"]
      if session["provider"] == 'twitter'
        p "twitter"
        client = Client.make(session["token"], session["secret"])
        t = TwitterGetData.new client
        t.run()
      elsif session["provider"] == 'facebook'
        p "facebook"
        client = FacebookClient.make(session["token"])
        f = FacebookGetData.new client
        f.run()
      end
      p "background end"
    rescue => exc
      p exc
    end
  end
end
