class FeedsController < ApplicationController
  before_filter :authenticate_user!
  # GET /feeds
  # GET /feeds.json
  def index
    Resque.enqueue(RunProcessor, session) 
    @feeds = current_user.feeds.all(:order => "last_modified")
    @entries = []
    _entries = []
    if params[:star] != nil
        _ids = [-1]
        current_user.stared_entries.each do |star|
          _ids << star.entry_id
        end
        @feeds.each do |feed|
          if params[:star] == "1"
            _entries.concat(feed.entries.find(:all, :order => "published DESC", :conditions => ["id in (?)", _ids]))
          else
            _entries.concat(feed.entries.find(:all, :order => "published DESC", :conditions => ["id not in (?)", _ids]))
          end
        end
    elsif params[:read] != nil
        _ids = [-1]
        current_user.read_entries.each do |star|
          _ids << star.entry_id
        end
        @feeds.each do |feed|
          if params[:read] == "1"
            _entries.concat(feed.entries.find(:all, :order => "published DESC", :conditions => ["id in (?)", _ids]))
          else
            _entries.concat(feed.entries.find(:all, :order => "published DESC", :conditions => ["id not in (?)", _ids]))
          end
        end
    else
        _ids = [-1]
        current_user.read_entries.each do |read|
          _ids << read.entry_id
        end
        @feeds.each do |feed|
            _entries.concat(feed.entries.all(:limit => 5, :order => "published DESC", :conditions => ["id not in (?)", _ids])) 
        end
    end

    @entries = Kaminari.paginate_array(_entries).page(params[:page]).per(5)

    @newfeed = Feed.new

    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.ping
    friendsIds = $redis.smembers current_user.uid.to_s+"_edges"
    logger.debug(friendsIds)

    @friends = User.find(:all, :conditions => ["uid in (?) and provider = ?", friendsIds, "twitter"])
    logger.debug(@friends)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @feeds }
      format.js

    end
  end

  # GET /feeds/1
  # GET /feeds/1.json
  def show
    @feeds   = current_user.feeds
    @feed    = @feeds.find(params[:id])

    # filter by star
    if params[:star] != nil
      _ids = [-1]
      current_user.stared_entries.each do |star|
        _ids << star.entry_id
      end
      logger.debug(_ids)
      if params[:star] == "1"
        @entries = @feed.entries.page(params[:page]).per(5).order("published DESC").where("id in (?)", _ids)
      else
        @entries = @feed.entries.page(params[:page]).per(5).order("published DESC").where("id not in (?)", _ids)
      end
    # filter by read
    elsif params[:read] != nil
      _ids = [-1]
      current_user.read_entries.each do |read|
        _ids << read.entry_id
      end
      logger.debug(_ids)
      if params[:read] == "1"
        @entries = @feed.entries.page(params[:page]).per(5).order("published DESC").where("id in (?)", _ids)
      else
        @entries = @feed.entries.page(params[:page]).per(5).order("published DESC").where("id not in (?)", _ids)
      end
    # default
    else
      _ids = [-1]
      current_user.read_entries.each do |read|
        _ids << read.entry_id
      end
      @entries = @feed.entries.page(params[:page]).per(5).order("published DESC").where("id not in (?)", _ids)
    end

    @newfeed = Feed.new

    $redis = Redis.new(:host => 'localhost', :port => 6379)
    $redis.ping
    friendsIds = $redis.smembers current_user.uid.to_s+"_edges"
    logger.debug(friendsIds)

    @friends = User.find(:all, :conditions => ["uid in (?) and provider = ?", friendsIds, "twitter"])
    logger.debug(@friends)

    respond_to do |format|
      format.html { render :action => "index" and return }# show.html.erb
      format.json { render json: @feed }
      format.js
    end
  end

  # GET /feeds/new
  # GET /feeds/new.json
  def new
    @newfeed = Feed.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @feed }
    end
  end

  # GET /feeds/1/edit
  def edit
    @feed = Feed.find(params[:id])
  end

  # POST /feeds
  # POST /feeds.json
  def create
    _feeds = []
    _feed = Feedzirra::Feed.fetch_and_parse(params[:feed]["feed_url"])
    logger.debug("[Debug]"+_feed.title)
    _feeds << Feed.new(:feed_url => _feed.feed_url, :site_url => _feed.url, :title => _feed.title, :etag => _feed.etag, :last_modified => _feed.last_modified)


    respond_to do |format|
      if Feed.import(_feeds, { :on_duplicate_key_update => [:title, :site_url, :etag, :last_modified ] })
        @feed = Feed.find_by_feed_url(_feed.feed_url)
        logger.debug(@feed)

        _user_feeds = []
        _user_feeds << UserFeed.new(:user_id => current_user.id, :feed_id => @feed.id)
        UserFeed.import(_user_feeds)

        # entries bulk insert
        _entries = [] #Array.newでも可
        _feed.entries.each do |entry|
          logger.debug(entry.summary)
            _entries << Entry.new(:url => entry.url, :content => ActionView::Base.white_list_sanitizer.sanitize(entry.content), :summary => entry.summary, :author => entry.author, :categories => entry.categories, :title => entry.title, :published => entry.published, :feed_id => @feed.id)
        end

        if Entry.import(_entries, { :on_duplicate_key_update => [:title, :published, :author, :summary, :content, :categories, :feed_id]})
          logger.debug("[Debug] feed.id : "+@feed.id.to_s)
          format.html { redirect_to @feed, notice: 'Feed was successfully created.' }
          format.json { render json: @feed, status: :created, location: @feed }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end
  end

  def xml_upload
    _feeds = []
    _rawFeeds = []
    xml_file =  params[:feed][:xml].read
    doc = Nokogiri::XML(xml_file)
    logger.debug(doc)
    doc.root.elements.each do |node|
        if node.node_name.eql? 'body'
            node.elements.each do |node|
                logger.debug(node)
                next if !node.attr("xmlUrl") || node.attr("xmlUrl").match(/^http:\/\/www.google.com\/reader/)
                _feed = Feedzirra::Feed.fetch_and_parse(node.attr("xmlUrl"))
                next if !_feed || !_feed.title || !_feed.url
                _feeds << Feed.new(:feed_url => _feed.feed_url, :site_url => _feed.url, :title => _feed.title, :etag => _feed.etag, :last_modified => _feed.last_modified)
                _rawFeeds << _feed
            end
        end
    end

    respond_to do |format|
      if Feed.import(_feeds, { :on_duplicate_key_update => [:title, :site_url, :etag, :last_modified ] })
        _user_feeds = []
        _entries = []

        _rawFeeds.each do |_feed|
            @feed = Feed.find_by_feed_url(_feed.feed_url)
            logger.debug(@feed)
            _user_feeds << UserFeed.new(:user_id => current_user.id, :feed_id => @feed.id)


            _feed.entries.each do |entry|
              logger.debug(entry.summary)
                _entries << Entry.new(:url => entry.url, :content => ActionView::Base.white_list_sanitizer.sanitize(entry.content), :summary => entry.summary, :author => entry.author, :categories => entry.categories, :title => entry.title, :published => entry.published, :feed_id => @feed.id)
            end
        end

        UserFeed.import(_user_feeds)


        # entries bulk insert
        if Entry.import(_entries, { :on_duplicate_key_update => [:title, :published, :author, :summary, :content, :categories, :feed_id]})
          logger.debug("[Debug] feed.id : "+@feed.id.to_s)
          format.html { redirect_to "/", notice: 'Feed was successfully created.' }
          format.json { render json: @feed, status: :created, location: @feed }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end

  end

  # PUT /feeds/1
  # PUT /feeds/1.json
  def update
    @feed = Feed.find(params[:id])

    respond_to do |format|
      if @feed.update_attributes(params[:feed])
        format.html { redirect_to @feed, notice: 'Feed was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @feed.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /feeds/1
  # DELETE /feeds/1.json
  def destroy
    @feed = Feed.find(params[:id])
    @feed.destroy

    respond_to do |format|
      format.html { redirect_to feeds_url }
      format.json { head :no_content }
    end
  end
  def unsubscribe
      @feed = Feed.find(params[:id])
      @user_feed = current_user.user_feeds.find_by_feed_id(params[:id])
      logger.debug(@user_feed)
      @user_feed.destroy if @user_feed

      render json: {feed: @feed, type: "unsubscribe"}

  end

  def get_data
    begin
      @client = Client.make(session[:token], session[:secret])
      #Twitter.followers('azihsoyn'))
      t = TwitterGetData.new @client
      friends = User.find_by_uid(t.getEdgesFromDB(@client.current_user.id), :conditions => ["provider = ?", "twitter"])
      logger.debug(friends)
      Resque.enqueue(RunProcessor, session) 
      data = @client.current_user #なんか取ってきます
      logger.debug(data)
      render :json => data
    rescue => exc
      render :json => exc
    end
  end

end
