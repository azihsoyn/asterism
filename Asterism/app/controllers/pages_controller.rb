class PagesController < ApplicationController
  layout "login"
  def login
	  if user_signed_in?
		  redirect_to :controller => "feeds", :action => "index"
	  end
  end
end
