class ChangeTypeToEntries < ActiveRecord::Migration
  def up
	  change_column :entries, :content, :text, :character_set => "utf8mb4"
	  change_column :entries, :summary, :text, :character_set => "utf8mb4"
	  change_table(:entries, :options => 'CHARSET=utf8mb4') do |t|
	  end
  end

  def down
	  change_column :entries, :content, :text, :character_set => "utf8"
	  change_column :entries, :summary, :text, :character_set => "utf8"
	  change_table(:entries, :options => 'CHARSET=utf8') do |t|
	  end
  end
end
