class AddUniqueIndexToUserFeeds < ActiveRecord::Migration
  def change
	  add_index :user_feeds, [:user_id, :feed_id], :unique => true
  end
end
