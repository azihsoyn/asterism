class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.string :title
      t.string :url
      t.datetime :published
      t.string :author
      t.text :summary
      t.text :content
      t.integer :feed_id
      t.string :categories

      t.timestamps
    end
  end
end
