class AddUniqueIndexUrlToEntries < ActiveRecord::Migration
  def change
	  add_index :entries, :url, :unique => true
  end
end
