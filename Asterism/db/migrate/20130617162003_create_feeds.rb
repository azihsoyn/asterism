class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.integer :id
      t.string :title
      t.string :site_url
      t.string :feed_url
      t.string :etag
      t.datetime :last_modified
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
