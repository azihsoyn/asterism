class CreateUserFeeds < ActiveRecord::Migration
  def change
    create_table :user_feeds do |t|
      t.integer :id
      t.integer :user_id
      t.integer :feed_id
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end

  end
end
