class CreateStaredEntries < ActiveRecord::Migration
  def change
    create_table :stared_entries do |t|
      t.integer :id
	  t.integer :user_id
	  t.integer :entry_id
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end

  end
end
