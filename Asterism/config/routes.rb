Asterism::Application.routes.draw do
  get "pages/login"
  get "feeds/get_data"

  post   "feeds/xml_upload",       to: "feeds#xml_upload",  as: "xml_upload"
  delete "/feeds/unsubscribe/:id", to: "feeds#unsubscribe", as: "unsubscribe"

  get    "/entries/star/:id",    to: "entries#star",   as: "star_entry"
  delete "/entryies/unstar/:id", to: "entries#unstar", as: "unstar_entry"

  get    "/entries/read/:id",    to: "entries#read",   as: "read_entry"
  delete "/entryies/unread/:id", to: "entries#unread", as: "unread_entry"

  get    "/friend_feeds/:friend_id",           to: "friend_feeds#index", as: "friend_feeds_index"
  get    "/friend_feeds/:friend_id/:feed_id",  to: "friend_feeds#show",  as: "friend_feeds_show"

  #devise_for :users

  resources :entries

  resources :feeds

  root :to => 'pages#login'

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }

  #devise_scope :user do
  #  get 'sign_in', :to => 'devise/sessions#new', :as => :new_user_session
  #  get 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
  #end



  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
